#!/usr/bin/env ruby
# test code for Citrusbyte Co.
#
require "minitest/autorun"

class Array
  @exam = false
  def exam n
=begin
for this step (flatten array) I used an primitive methods of ruby "flatten and combination"
=end
    @flatten = self.flatten
    @combination = @flatten.combination(2).to_a

    @combination.each { |slice| @exam = @exam || (slice[0].to_i + slice[1].to_i == n.to_i) }
    @exam
  end
end

class TestExam < Minitest::Test
  def original
    @original = [[30, 501, [22]], 4, [38, 2]]
    assert_equal true, @original.exam(531)
  end
  def test_22
    @by_test = [1, [[20],[[43],[[10], [99]]], [5],[6]]]
    assert_equal false, @by_test.exam(22)
  end
  def test_21
    @by_test = [1, [[20],[[43],[[10], [99]]], [5],[6]]]
    assert_equal true, @by_test.exam(21)
  end
  def test_104
    @by_test = [1, [[20],[[43],[[10], [99]]], [5],[6]]]
    assert_equal true, @by_test.exam(104)
  end
  def test_111
    @by_test = [1, [[20],[[43],[[10], [99]]], [5],[6]]]
    assert_equal false, @by_test.exam(111)
  end

end
